/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shapeproject;

/**
 *
 * @author Satit Wapeetao
 */
public class Triangle {
    private double H,B;
    public static final double pi=0.5;
    public Triangle(){
        this.H=H;
        this.B=B;
    }
    public double CalArea(){
        return pi*H*B;
    }
    public double getRH(){
        return H;
    }
    public double getRB(){
        return B;
    }
    public void setR(double H,double B){
        if(H<=0||B<=0) return;
        this.H=H;
        this.B=B;
    }
}
