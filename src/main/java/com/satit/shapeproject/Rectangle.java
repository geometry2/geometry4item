/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shapeproject;

import java.awt.BorderLayout;

/**
 *
 * @author Satit Wapeetao
 */
public class Rectangle {
    private double w,l;

    public double CalArea (){
        return w*l;
    }
    public double getRw(){
        return w;
             
    }
     public double getRl(){
        return l;
             
    }
    public void setR(double w,double l){
        if(w<=0||l<=0)
            return;         
        this.w=w;
        this.l=l;
    }
}
