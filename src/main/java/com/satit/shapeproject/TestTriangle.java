/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shapeproject;

/**
 *
 * @author Satit Wapeetao
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1=new Triangle();
        triangle1.setR(5.5,7);
        System.out.println("Area of Triangle "+"(H = "+triangle1.getRH()+"(, B = "+triangle1.getRB()+") is "+triangle1.CalArea());
        triangle1.setR(0,0);
        System.out.println("Area of Triangle "+"(H = "+triangle1.getRH()+"(, B = "+triangle1.getRB()+") is "+triangle1.CalArea());
    }
}
