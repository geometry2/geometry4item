/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.shapeproject;

/**
 *
 * @author Satit Wapeetao
 */
public class Circle {
    private double r;
    public static final double pi =22.0/7;
    public Circle(double r){
        this.r=r;
    }
    public double CalArea(){
        return pi * r *r;
    }
    public double getR(){
        return r;
    }
    public void setR(double r){
        if(r<=0) return;
        this.r=r;
    }
    
}
